using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace QuangARKit
{
    public class ARObjectManager : MonoBehaviour
    {
        private int objectID;
        private Vector3 ARSpacePosition;
        private Quaternion ARSpaceRotation;
        private Vector3 ARSpaceScale;
        private bool isSelected;

        public void SetProperties(int id, Vector3 position, Quaternion rotation, Vector3 scale)
        {
            objectID = id;
            ARSpacePosition = position;
            ARSpaceRotation = rotation;
            ARSpaceScale = scale;
        }

        public int GetObjectID()
        {
            return objectID;
        }

        public Vector3 GetPosition()
        {
            return ARSpacePosition;
        }

        public Quaternion GetRotation()
        {
            return ARSpaceRotation;
        }

        public Vector3 GetScale()
        {
            return ARSpaceScale;
        }

        public bool IsSelected()
        {
            return isSelected;
        }

        public void SetSelectedState(bool isSelected)
        {
            this.isSelected = isSelected;
        }

    }
}

