

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.XR.ARSubsystems;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using Photon.Pun;

public class ARSceneManager : MonoBehaviour
{
    [SerializeField] ARSession m_Session;
    [SerializeField] ARCameraManager m_CameraManager;


    IEnumerator Start()
    {
        if ((ARSession.state == ARSessionState.None) ||
            (ARSession.state == ARSessionState.CheckingAvailability))
        {
            yield return ARSession.CheckAvailability();
        }

        if (ARSession.state == ARSessionState.Unsupported)
        {
            //GameData.isARvalid = false;
            loadScene(0);
        }
        else
        {
            // Start the AR session
            m_Session.enabled = true;
        }
    }

    public void ChangeCamera()
    {
        m_CameraManager.requestedFacingDirection = CameraFacingDirection.World;
    }    

    public void Back()
    {
        PhotonNetwork.Disconnect();
        Destroy(NetworkController.Instance.gameObject);
        SceneManager.LoadScene(0);
    }

    public void Exit()
    {
#if UNITY_IOS
        Application.Unload();
#elif UNITY_ANDROID
        Application.Quit();
#elif UNITY_STANDALONE_WIN
        Application.Quit();
#endif
    }

    public void loadScene(int index)
    {
        SceneManager.LoadScene(index);
    }

}
