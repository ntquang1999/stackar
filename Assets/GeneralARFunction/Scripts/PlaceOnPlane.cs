﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using UnityEngine.UI;
using System;

namespace QuangARKit
{
    [RequireComponent(typeof(ARRaycastManager))]
    public class PlaceOnPlane : MonoBehaviour
    {
        [SerializeField] GameObject objectToPlacePrefab;
        [SerializeField] GameObject indicatorPrefab;
        [SerializeField] GameObject moveAnim;
        [SerializeField] GameObject placeAnim;

        public GameObject spawnedObject { get; private set; }
        public bool objectPlaced = false;
        public event Action<bool> OnObjectPlaced;
        bool posValid = false;
        static List<ARRaycastHit> s_Hits = new List<ARRaycastHit>();
        ARRaycastManager m_RaycastManager;
        public GameObject placedPrefab
        {
            get { return objectToPlacePrefab; }
            set { objectToPlacePrefab = value; }
        }

        void Awake()
        {
            m_RaycastManager = GetComponent<ARRaycastManager>();
        }

        bool TryGetTouchPosition(out Vector2 touchPosition)
        {
            if (Input.touchCount > 0)
            {
                touchPosition = Input.GetTouch(0).position;
                return true;
            }

            touchPosition = default;
            return false;
        }

        public void ResetGame()
        {
            DestroyImmediate(spawnedObject);
            objectPlaced = false;
            OnObjectPlaced?.Invoke(false);
            GetComponent<ARPointCloudManager>().SetTrackablesActive(true);
            GetComponent<ARPlaneManager>().SetTrackablesActive(true);
        }    

        private void CastIndicator()
        {
            if (!objectPlaced && m_RaycastManager.Raycast(Camera.current.ViewportToScreenPoint(new Vector3(0.5f, 0.5f)), s_Hits, TrackableType.PlaneWithinPolygon))
            {
                moveAnim.SetActive(false);
                placeAnim.SetActive(true);
                var hitPose = s_Hits[0].pose;

                if (spawnedObject == null)
                {
                    spawnedObject = Instantiate(indicatorPrefab, hitPose.position, objectToPlacePrefab.transform.rotation);
                }
                else
                {
                    spawnedObject.transform.SetPositionAndRotation(hitPose.position, hitPose.rotation);
                }

                posValid = true;
            }
        }   
        
        private void PlaceObject()
        {
            if (posValid)
            {
                if (!objectPlaced)
                {
                    DestroyImmediate(spawnedObject);
                }
                var hitPose = s_Hits[0].pose;

                if (spawnedObject == null)
                {
                    objectPlaced = true;
                    OnObjectPlaced?.Invoke(true);
                    spawnedObject = Instantiate(objectToPlacePrefab, hitPose.position, hitPose.rotation);
                    GetComponent<ARPointCloudManager>().SetTrackablesActive(false);
                    GetComponent<ARPlaneManager>().SetTrackablesActive(false);
                }
                placeAnim.SetActive(false);
            }
        }    

        void Update()
        {
            CastIndicator();
            if (!TryGetTouchPosition(out Vector2 touchPosition))
                return;
            PlaceObject();
        }
    }
}
