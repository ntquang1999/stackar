using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace QuangARKit
{
    public class NativeMessageHandler
    {
        private static NativeMessageHandler instance;

        private NativeMessageHandler()
        {

        }

        public static NativeMessageHandler GetInstance()
        {
            if (instance == null)
                instance = new NativeMessageHandler();
            return instance;
        }

        private Dictionary<int, INativeMessager> senderList = new Dictionary<int, INativeMessager>();
        private int index = 0;

        public void SendMessage(string message, INativeMessager component)
        {
            if (Application.platform == RuntimePlatform.Android)
            {
                using (AndroidJavaClass jc = new AndroidJavaClass("com.QuangARKit.Communication.UnityActivity"))
                {
                    jc.CallStatic("onUnityMessage", message + ":" + index);
                }
            }
            else if (Application.platform == RuntimePlatform.IPhonePlayer)
            {
#if UNITY_IOS && !UNITY_EDITOR
            NativeAPI.sendMessageToMobileApp("exit");
#endif
            }
            senderList.Add(index, component);
            index++;
        }

        public void MessageReceiver(string message)
        {
            string[] messageParts = message.Split(":");
            int key = Int32.Parse(messageParts[messageParts.Length - 1]);
            senderList[key].HandleMessage(message);
            senderList.Remove(key);
        }

    }

}
