using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace QuangARKit
{

    public interface INativeMessager
    {
        protected virtual void SendMessage(string message)
        {
            NativeMessageHandler.GetInstance().SendMessage(message, this);
        }

        public abstract void HandleMessage(string message);

    }
}

