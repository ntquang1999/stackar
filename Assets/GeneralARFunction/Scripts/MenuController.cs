using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{

    public bool buildingForReactNative = false;
    public bool buildingForFlutter = false;
    public Text nativeMessage;

    public void LoadScene(int index)
    {
        SceneManager.LoadScene(index);
    }

    public void Exit()
    {
        if(buildingForReactNative)
        {
            var buttonHandler = GetComponent<ReactNativeMessageHandler>();
            buttonHandler.Exit();
            return;
        }
        if(buildingForFlutter)
        {
            var flutterMessageHandler = GetComponent<FlutterMessageHandler>();
            flutterMessageHandler.SendMessageToFlutter("exit");
            return;
        }
#if UNITY_IOS
        Application.Unload();
#elif UNITY_ANDROID
        Application.Quit();
#elif UNITY_STANDALONE_WIN
        Application.Quit();
#endif
    }

    public void MessageReceiver(string message)
    {
        nativeMessage.text = message;
    }    

}
