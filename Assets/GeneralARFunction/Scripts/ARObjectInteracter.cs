using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace QuangARKit
{
    [RequireComponent(typeof(ARObjectManager))]
    public class PlacementRotation : MonoBehaviour
    {
        private ARObjectManager placementObject;

        [SerializeField]
        private Vector3 rotationSpeed = Vector3.zero;

        void Awake()
        {
            placementObject = GetComponent<ARObjectManager>();
        }

        void Update()
        {
            if (placementObject.IsSelected())
            {
                transform.Rotate(rotationSpeed * Time.deltaTime, Space.World);
            }
        }
    }
}

