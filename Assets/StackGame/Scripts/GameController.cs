using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using DG.Tweening;
using SimpleJSON;
using QuangARKit;
using System;

public class GameController : MonoBehaviour
{
    public static GameController Instance;

    public event Action<int> OnAddPoints;

    [SerializeField] GameObject stackPref;
    [SerializeField] GameObject baseStack;
    [SerializeField] Transform parent;
    [SerializeField] Transform camera;
 
    [SerializeField] float perfectThreshold = 0.1f;
    [SerializeField] float gravity = -1f;


    GameObject currentStack;
    GameObject lastStack;
    StackController currentStackController;
    ColorGenerator colorGenerator;

    private bool playing = false;
    private bool leftside = true;
    private bool firstStack = true;
    private int combo = 0;
    private bool endgame = false;
    private int points = 0;


    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else Destroy(gameObject);

        Application.targetFrameRate= 60;
        Physics.gravity = new Vector3(0, gravity, 0);
        colorGenerator = GetComponent<ColorGenerator>();

        
        
    }

    void Start()
    {
        //Initiate base stack
        lastStack = baseStack;
        baseStack.GetComponent<Renderer>().material.color = colorGenerator.getCurrentColor();
    }

    
    void Update()
    {
        
        if(!endgame)
        {
            checkForSpawn();
            checkForInput();
        }
        else
        {
            
            StartCoroutine(ResetGameAfterDelay(2));
        }
    }

    public void AddPoint(int amount)
    {
        points += amount;
        OnAddPoints.Invoke(points);
    }

    public int GetPoint()
    {
        return points;
    }



    void checkForSpawn()
    {
        if (!playing)
        {
            if(firstStack)
            {
                currentStack = Instantiate(stackPref, parent);
                firstStack = false;
            }
            else currentStack = Instantiate(lastStack, parent);
            currentStack.GetComponent<Renderer>().material.color = colorGenerator.getNextColor();
            currentStack.transform.localPosition = new Vector3(
                lastStack.transform.localPosition.x,
                lastStack.transform.localPosition.y + lastStack.transform.localScale.z / 100,
                lastStack.transform.localPosition.z
            );
    
            

            currentStackController = currentStack.GetComponent<StackController>();
            currentStackController.Reset();
            currentStackController.LeftSide = leftside;
            
            playing = true;
        }
    }

    void checkForInput()
    {
        if(Input.GetKeyDown(KeyCode.Space) || (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began))
        {
            if (leftside)
            {
                float offset = lastStack.transform.localPosition.x - currentStack.transform.localPosition.x;
                if (Mathf.Abs(offset) <= currentStack.transform.localScale.x/10)
                {
                    AddPoint(1);
                    slashStackX();
                    lastStack = currentStack;
                    leftside = !leftside;
                    playing = false;
                }
                else
                {
                    endgame = true;
                    currentStackController.Drop();
                }
            }
            else
            {
                float offset = lastStack.transform.localPosition.z - currentStack.transform.localPosition.z;
                if (Mathf.Abs(offset) <= currentStack.transform.localScale.y/10)
                {
                    AddPoint(1);
                    slashStackZ();
                    lastStack = currentStack;
                    leftside = !leftside;
                    playing = false;
                }
                else
                {
                    endgame = true;
                    currentStackController.Drop();
                }
            }

            //camera.GetComponent<CameraController>().targetPosition = new Vector3(camera.transform.position.x, currentStack.transform.localPosition.y + 3f, camera.transform.position.z);

        }
    }

    void slashStackX()
    {
        float offset = lastStack.transform.localPosition.x - currentStack.transform.localPosition.x;
        float offsetAbs = Mathf.Abs(offset);
        if(offsetAbs<= perfectThreshold)
        {
            currentStack.transform.localPosition = new Vector3(
                lastStack.transform.localPosition.x, 
                currentStack.transform.localPosition.y, 
                lastStack.transform.localPosition.z
            );
            currentStackController.movingAlowed = false;
            combo++;
            currentStackController.MakeSpark(combo);
            AudioController.Instance.PlayPerfectSound(combo);
        }
        else if(offset > 0)
        {
            var drop = Instantiate(currentStack, parent);
            var dropController = drop.GetComponent<StackController>();
            dropController.Drop();
            dropController.SlashRightX(drop.transform.localScale.x - offsetAbs * 10);

            currentStackController.SlashLeftX(offsetAbs*10);
            currentStackController.movingAlowed = false;
            combo = 0;
            AudioController.Instance.PlayStackSound();
        }
        else
        {
            var drop = Instantiate(currentStack, parent);
            var dropController = drop.GetComponent<StackController>();
            dropController.Drop();
            dropController.SlashLeftX(drop.transform.localScale.x - offsetAbs * 10);

            currentStackController.SlashRightX(offsetAbs*10);
            currentStackController.movingAlowed = false;
            combo = 0;
            AudioController.Instance.PlayStackSound();
        }
        
    }

    void slashStackZ()
    {
        float offset = lastStack.transform.localPosition.z - currentStack.transform.localPosition.z;
        float offsetAbs = Mathf.Abs(offset);
        if (offsetAbs <= perfectThreshold)
        {
            currentStack.transform.localPosition = new Vector3(
                lastStack.transform.localPosition.x,
                currentStack.transform.localPosition.y,
                lastStack.transform.localPosition.z
            );
            currentStackController.movingAlowed = false;
            combo++;
            currentStackController.MakeSpark(combo);
            AudioController.Instance.PlayPerfectSound(combo);
        }
        else if (offset > 0)
        {
            var drop = Instantiate(currentStack, parent);
            var dropController = drop.GetComponent<StackController>();
            dropController.Drop();
            dropController.SlashLeftY(drop.transform.localScale.y - offsetAbs * 10);


            currentStackController.SlashRightY(offsetAbs * 10);
            currentStackController.movingAlowed = false;
            combo = 0;
            AudioController.Instance.PlayStackSound();
        }
        else
        {
            var drop = Instantiate(currentStack, parent);
            var dropController = drop.GetComponent<StackController>();
            dropController.Drop();
            dropController.SlashRightY(drop.transform.localScale.y - offsetAbs * 10);

            currentStackController.SlashLeftY(offsetAbs * 10);
            currentStackController.movingAlowed = false;
            combo = 0;
            AudioController.Instance.PlayStackSound();
        }

    }


    public void LoadScene(int index)
    {
        SceneManager.LoadSceneAsync(index);
    }



    IEnumerator ResetGameAfterDelay(float delay)
    {
        yield return new WaitForSeconds(delay);
        var placeOnPlane = FindObjectOfType<PlaceOnPlane>();
        placeOnPlane.ResetGame();
        points = 0;
    }
}
