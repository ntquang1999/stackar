using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorGenerator : MonoBehaviour
{
    private int red = 0;
    private int green = 255;
    private int blue = 10;
    private string rgbRotation = "b";
    private string direction = "dec";
    public int speed = 5;

    private void Awake()
    {
        int selection = Random.Range(0, 4);
        switch(selection)
        {
            case 0:
                red = 0;
                green = 255;
                blue = 10;
                rgbRotation = "b";
                direction = "dec";
                break;
            case 1:
                red = 255;
                green = 0;
                blue = 0;
                rgbRotation = "b";
                direction = "inc";
                break;
            case 2:
                red = 255;
                green = 0;
                blue = 255;
                rgbRotation = "r";
                direction = "dec";
                break;
            case 3:
                red = 0;
                green = 0;
                blue = 255;
                rgbRotation = "g";
                direction = "inc";
                break;
            case 4:
                red = 0;
                green = 139;
                blue = 255;
                rgbRotation = "g";
                direction = "inc";
                break;
        }
    }

    public void changeRotation()
    {
        Debug.Log(rgbRotation == "b");
        if (rgbRotation == "b")
        {
            rgbRotation = "r";
            return;
        }
            
        if (rgbRotation == "r")
        {
            rgbRotation = "g";
            return;
        }
        if (rgbRotation == "g")
        {
            rgbRotation = "b";
            return;
        }
    }

    public void changeDirection()
    {
        if (direction == "dec")
            direction = "inc";
        else
            direction = "dec";
    }

    public Color getCurrentColor()
    {
        return new Color(red / 255f, green / 255f, blue / 255f);
    }

    public Color getNextColor()
    {
        switch(rgbRotation)
        {
            case "r":
                if(direction == "inc")
                {
                    if (red < 255)
                        add(ref red);
                    else
                    {
                        changeRotation();
                        changeDirection();
                        return getNextColor();
                    } 
                        
                }
                if (direction == "dec")
                {
                    if (red > 0)
                        subtract(ref red);
                    else
                    {
                        changeRotation();
                        changeDirection();
                        return getNextColor();
                    }

                }
                break;
            case "g":
                if (direction == "inc")
                {
                    if (green < 255)
                        add(ref green);
                    else
                    {
                        changeRotation();
                        changeDirection();
                        return getNextColor();
                    }

                }
                if (direction == "dec")
                {
                    if (green > 0)
                        subtract(ref green);
                    else
                    {
                        changeRotation();
                        changeDirection();
                        return getNextColor();
                    }

                }
                break;
            case "b":
                if (direction == "inc")
                {
                    if (blue < 255)
                        add(ref blue);
                    else
                    {
                        changeRotation();
                        changeDirection();
                        return getNextColor();
                    }

                }
                if (direction == "dec")
                {
                    if (blue > 0)
                        subtract(ref blue);
                    else
                    {
                        changeRotation();
                        changeDirection();
                        Debug.Log(rgbRotation);
                        return getNextColor();
                    }

                }
                break;
        }
        //Debug.Log(red + "; " + green + "; " + blue);
        return new Color(red / 255f, green / 255f, blue / 255f);
    }

    private void subtract(ref int rgb)
    {
        if (rgb - speed < 0) rgb = 0;
        else rgb = rgb - speed;
    }
    private void add(ref int rgb)
    {
        if (rgb + speed > 255) rgb = 255;
        else rgb = rgb + speed;
    }
}
