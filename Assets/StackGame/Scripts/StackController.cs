using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StackController : MonoBehaviour
{
    [SerializeField] float cutOffset = 0.1f;
    [SerializeField] GameObject sparkPref;
    [SerializeField] float speed = 3;

    public bool LeftSide = true;
    public bool movingAlowed = true;
    

    [HideInInspector] public Material material;
    [HideInInspector] public Material materialEdgeLeft;
    [HideInInspector] public Material materialEdgeRight;
    [HideInInspector] public Material materialEdgeLeft2;
    [HideInInspector] public Material materialEdgeRight2;

    private Vector3 anchorPosition;
    private Vector3 edgeXPositive;
    private Vector3 edgeXNegative;
    private Vector3 edgeZPositive;
    private Vector3 edgeZNegative;
    private bool goingForward = true;
    private bool isDrop = false;


    private void Start()
    {

        anchorPosition = transform.localPosition;
        edgeXPositive = new Vector3(anchorPosition.x + (transform.localScale.x / 10) + 0.3f, anchorPosition.y, anchorPosition.z);
        edgeZPositive = new Vector3(anchorPosition.x, anchorPosition.y, anchorPosition.z + (transform.localScale.y / 10) + 0.3f);
        edgeXNegative = new Vector3(anchorPosition.x - (transform.localScale.x / 10) - 0.3f, anchorPosition.y, anchorPosition.z);
        edgeZNegative = new Vector3(anchorPosition.x, anchorPosition.y, anchorPosition.z - (transform.localScale.y / 10) - 0.3f);

        if (isDrop) return;

        if (LeftSide)
        {
            transform.localPosition = edgeXPositive;
        }
        else
        {
            transform.localPosition = edgeZPositive;
        }
    }

    private void Update()
    {
        if (isDrop) return;
        move();
    }

    void move()
    {
        if (!movingAlowed) return;
        if(LeftSide)
        {
            if(goingForward)
            {
                transform.localPosition = Vector3.MoveTowards(transform.localPosition, edgeXNegative, speed/1000);
            }
            else
            {
                transform.localPosition = Vector3.MoveTowards(transform.localPosition, edgeXPositive, speed/1000);
            }
            if (Mathf.Abs(transform.localPosition.x - edgeXNegative.x) <= 0.0005f || 
                Mathf.Abs(transform.localPosition.x - edgeXPositive.x) <= 0.0005f)
            {
                goingForward = !goingForward;
            }
        }
        else
        {
            if (goingForward)
            {
                transform.localPosition = Vector3.MoveTowards(transform.localPosition, edgeZNegative, speed / 1000);
            }
            else
            {
                transform.localPosition = Vector3.MoveTowards(transform.localPosition, edgeZPositive, speed / 1000);
            }
            if (Mathf.Abs(transform.localPosition.z - edgeZNegative.z) <= 0.0005f ||
                Mathf.Abs(transform.localPosition.z - edgeZPositive.z) <= 0.0005f)
            {
                goingForward = !goingForward;
            }
        }
    }   
    
    public void Reset()
    {
        movingAlowed = true;
        goingForward = true;
    }

    public void MakeSpark(int combo)
    {
        if(combo <= 3)
        {
            GameController.Instance.AddPoint(1);
            showSpark(1);
        }
        else if(combo <= 6)
        {
            GameController.Instance.AddPoint(2);
            showSpark(1);
            StartCoroutine(SparkAfterDelay(0.08f, 2));
        }
        else if(combo <= 9)
        {
            GameController.Instance.AddPoint(3);
            showSpark(1);
            StartCoroutine(SparkAfterDelay(0.06f, 2));
            StartCoroutine(SparkAfterDelay(0.12f, 3));
        }
        else
        {
            GameController.Instance.AddPoint(4);
            showSpark(1);
            StartCoroutine(SparkAfterDelay(0.06f, 2));
            StartCoroutine(SparkAfterDelay(0.12f, 3));
            StartCoroutine(SparkAfterDelay(0.15f, 4));
        }

    }

    void showSpark(int time)
    {
        var spark = Instantiate(sparkPref, transform.parent);

        spark.GetComponent<Renderer>().material.color = Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);

        spark.transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y - 0.05f, transform.localPosition.z);

        spark.transform.localScale = new Vector3(
                transform.localScale.x/10 + 0.1f * time,
                transform.localScale.y/10 + 0.1f * time,
                transform.localScale.z/10 + 0.1f * time
            );
    }
    IEnumerator SparkAfterDelay(float delay, int time)
    {
        yield return new WaitForSeconds(delay);
        showSpark(time);
    }


    public void SlashRightX(float offset)
    {
        var scale = transform.localScale;
        var position = transform.localPosition;

        transform.localScale = new Vector3(scale.x-offset, scale.y, scale.z);
        transform.localPosition = new Vector3(position.x-(offset/20), position.y, position.z);

    }

    public void SlashLeftX(float offset)
    {
        var scale = transform.localScale;
        var position = transform.localPosition;

        transform.localScale = new Vector3(scale.x - offset, scale.y, scale.z);
        transform.localPosition = new Vector3(position.x + (offset / 20), position.y, position.z);

    }

    public void SlashRightY(float offset)
    {
        var scale = transform.localScale;
        var position = transform.localPosition;

        transform.localScale = new Vector3(scale.x, scale.y - offset, scale.z);
        transform.localPosition = new Vector3(position.x, position.y, position.z + (offset / 20));

    }

    public void SlashLeftY(float offset)
    {
        var scale = transform.localScale;
        var position = transform.localPosition;

        transform.localScale = new Vector3(scale.x, scale.y - offset, scale.z);
        transform.localPosition = new Vector3(position.x, position.y, position.z - (offset / 20));

    }

    public void Drop()
    {
        isDrop = true;
        GetComponent<Rigidbody>().isKinematic = false;
        //Destroy(gameObject, 3);
    }
}
