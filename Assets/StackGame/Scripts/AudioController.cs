using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour
{
    public static AudioController Instance;

    [SerializeField] AudioClip[] clips;
    AudioSource audioSource;
    private void Awake()
    {
        Instance = this;
        audioSource = GetComponent<AudioSource>();
    }

    public void PlayStackSound()
    {
        audioSource.PlayOneShot(clips[0]);
    }

    public void PlayPerfectSound(int i)
    {
        if (i > 5) i = 5;
        audioSource.PlayOneShot(clips[i]);
    }
}
