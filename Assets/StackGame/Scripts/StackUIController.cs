using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using QuangARKit;

public class StackUIController : MonoBehaviour
{
    [SerializeField] Text score;

    PlaceOnPlane placeOnPlane;
    GameController gameController;

    void Awake()
    {
        placeOnPlane = FindObjectOfType<PlaceOnPlane>();
        placeOnPlane.OnObjectPlaced += ObjectPlacedHandler;
    }

    
    void Update()
    {
        if (placeOnPlane.objectPlaced && gameController == null)
            registerForPoint();
        else
        {
            if (!placeOnPlane.objectPlaced)
                gameController = null;
            return;
        }

    }

    void registerForPoint()
    {
        gameController = FindObjectOfType<GameController>();
        gameController.OnAddPoints += updatePoint;
    }

    void updatePoint(int points)
    {
        score.text = points + "";
    }

    void ObjectPlacedHandler(bool objectPlaced)
    {
        score.text = "0";
        score.gameObject.SetActive(objectPlaced);
    }
}
