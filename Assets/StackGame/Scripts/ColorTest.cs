using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorTest : MonoBehaviour
{

    ColorGenerator colorGenerator;

    private void Awake()
    {
        colorGenerator = GetComponent<ColorGenerator>();
        gameObject.GetComponent<Renderer>().material.color = colorGenerator.getCurrentColor();
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            
            gameObject.GetComponent<Renderer>().material.color = colorGenerator.getNextColor();
            //Debug.Log(gameObject.GetComponent<Renderer>().material.color.r*255f +", " + gameObject.GetComponent<Renderer>().material.color.g * 255f + ", " + gameObject.GetComponent<Renderer>().material.color.b * 255f);
        }
    }
}
