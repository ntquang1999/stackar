using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using System;
using UnityEngine.SceneManagement;

public class NetworkController : MonoBehaviourPunCallbacks
{
    public static NetworkController Instance;

    public ChessboardController chessboard;
    public bool Connected = false;
    public bool GameStarted = false;
    public PlayerController localPlayer;
    public Side localSide;

    public event Action onJoinedLobby;
    public event Action onJoinedRoom;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }  
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        PhotonNetwork.ConnectUsingSettings();
    }

    public override void OnConnectedToMaster()
    {
        Debug.Log("Connected to server");
        PhotonNetwork.JoinLobby();
    }

    public override void OnJoinedLobby()
    {
        Debug.Log("Joined lobby");
        onJoinedLobby?.Invoke();
    }

    public override void OnJoinedRoom()
    {
        Debug.Log("Joined room");
        Connected = true;
        onJoinedRoom?.Invoke();
    }

    public void JoinRoomWithCode(string code)
    {
        RoomOptions roomOptions = new RoomOptions();
        roomOptions.MaxPlayers = 2;
        PhotonNetwork.JoinOrCreateRoom(code,roomOptions, null);
    }

    private void Update()
    {
        if (!Connected) return;
        if (GameStarted) return;
        if (PhotonNetwork.CurrentRoom.PlayerCount == 2)
        {
            GameStarted = true;
            PhotonNetwork.LoadLevel(2);
        }
            
    }

    public void StartGame()
    {
        chessboard.BoardCreated += BoardCreated;
        chessboard.StartGame();
    }

    public void BoardCreated(BoardManager board)
    {
        if (PhotonNetwork.LocalPlayer.IsMasterClient)
        {
            localSide = Side.light;
        }
        else
        {
            localSide = Side.dark;
            chessboard.gameObject.transform.rotation = Quaternion.Euler(chessboard.gameObject.transform.rotation.x, chessboard.gameObject.transform.rotation.y + 180, chessboard.gameObject.transform.rotation.z);
        }
        board.player = localPlayer;
        foreach(var photonView in FindObjectsOfType<PhotonView>())
        {
            photonView.RPC("SetProperties", RpcTarget.All);
            photonView.RPC("CacheBoardComponent", RpcTarget.All);
        }
    }
}
