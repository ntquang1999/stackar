using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cell : MonoBehaviour
{
    public int X;
    public int Y;
    public GameObject chessStanding;
    [SerializeField] Material selectedMaterial;
    [SerializeField] Material canKillEnemyMaterial;
    [SerializeField] Material enemyCanKillMaterial;

    public Vector3 GetPosition()
    {
        return new Vector3(X, 0, Y);
    }

    public ChessPos GetPos()
    {
        return new ChessPos(X, Y);
    }

    public void SetSelection(bool selected, int selectedType)
    {
        if(selected)
        {
            GetComponent<MeshRenderer>().enabled = true;
            switch(selectedType)
            {
                case 0:
                    GetComponent<Renderer>().material = selectedMaterial;
                    break;
                case 1:
                    GetComponent<Renderer>().material = canKillEnemyMaterial;
                    break;
                case 2:
                    GetComponent<Renderer>().material = enemyCanKillMaterial;
                    break;
            }
            
        }  
        else
        {
            GetComponent<MeshRenderer>().enabled = false;
        }     
    }
}
