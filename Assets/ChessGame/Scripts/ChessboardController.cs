using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChessboardController : MonoBehaviour
{
    [SerializeField] GameObject boardPrefab;

    BoardManager board;

    public event Action<BoardManager> BoardCreated;

    private void Awake()
    {
        NetworkController.Instance.chessboard = this;
        NetworkController.Instance.StartGame();
    }

    public void StartGame()
    {
        var boadObj = Instantiate(boardPrefab, transform);
        boadObj.transform.localPosition = new Vector3(-3.5f, 0, -3.5f);
        boadObj.transform.localRotation = Quaternion.identity;
        board = boadObj.GetComponent<BoardManager>();
        BoardCreated?.Invoke(board);
    }

    public void RestartGame()
    {
        Destroy(board.gameObject);

        StartGame();
    }
}
