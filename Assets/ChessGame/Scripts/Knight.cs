using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Knight : Chess
{
    public override void Awake()
    {
        base.Awake();
        type = ChessType.Knight;
    }
    public override List<ChessPos> CheckValidMove()
    {
        List<ChessPos> valid = new List<ChessPos>();
        
        CheckAndAddPosToList(new ChessPos(x + 1, y + 2), true, true, ref valid);
        CheckAndAddPosToList(new ChessPos(x + 2, y + 1), true, true, ref valid);
        CheckAndAddPosToList(new ChessPos(x - 1, y - 2), true, true, ref valid);
        CheckAndAddPosToList(new ChessPos(x - 2, y - 1), true, true, ref valid);
        CheckAndAddPosToList(new ChessPos(x + 1, y - 2), true, true, ref valid);
        CheckAndAddPosToList(new ChessPos(x + 2, y - 1), true, true, ref valid);
        CheckAndAddPosToList(new ChessPos(x - 1, y + 2), true, true, ref valid);
        CheckAndAddPosToList(new ChessPos(x - 2, y + 1), true, true, ref valid);

        return valid;

    }

    public override List<ChessPos> CheckValidKill()
    {
        List<ChessPos> valid = new List<ChessPos>();

        CheckAndAddPosToList(new ChessPos(x + 1, y + 2), true, true, ref valid, true);
        CheckAndAddPosToList(new ChessPos(x + 2, y + 1), true, true, ref valid, true);
        CheckAndAddPosToList(new ChessPos(x - 1, y - 2), true, true, ref valid, true);
        CheckAndAddPosToList(new ChessPos(x - 2, y - 1), true, true, ref valid, true);
        CheckAndAddPosToList(new ChessPos(x + 1, y - 2), true, true, ref valid, true);
        CheckAndAddPosToList(new ChessPos(x + 2, y - 1), true, true, ref valid, true);
        CheckAndAddPosToList(new ChessPos(x - 1, y + 2), true, true, ref valid, true);
        CheckAndAddPosToList(new ChessPos(x - 2, y + 1), true, true, ref valid, true);

        return valid;
    }


}
