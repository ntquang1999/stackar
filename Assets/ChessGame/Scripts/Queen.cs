using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Queen : Chess
{
    public override void Awake()
    {
        base.Awake();
        type = ChessType.Queen;
    }
    public override List<ChessPos> CheckValidMove()
    {
        List<ChessPos> valid = new List<ChessPos>();
        int x = this.x;
        int y = this.y;
        while(x <= 7 && y <= 7)
        {
            if (CheckAndAddPosToList(new ChessPos(x += 1, y += 1), true, true, ref valid))
                break;
        }
        x = this.x;
        y = this.y;
        while (x <= 7 && y >= 0)
        {
            if (CheckAndAddPosToList(new ChessPos(x += 1, y -= 1), true, true, ref valid))
                break;
        }
        x = this.x;
        y = this.y;
        while (x >= 0 && y <= 7)
        {
            if (CheckAndAddPosToList(new ChessPos(x -= 1, y += 1), true, true, ref valid))
                break;
        }
        x = this.x;
        y = this.y;
        while (x >= 0 && y >= 0)
        {
            if (CheckAndAddPosToList(new ChessPos(x -= 1, y -= 1), true, true, ref valid))
                break;
        }
        x = this.x;
        y = this.y;
        while (x <= 7)
        {
            if (CheckAndAddPosToList(new ChessPos(x += 1, y), true, true, ref valid))
                break;
        }
        x = this.x;
        y = this.y;
        while (y >= 0)
        {
            if (CheckAndAddPosToList(new ChessPos(x, y -= 1), true, true, ref valid))
                break;
        }
        x = this.x;
        y = this.y;
        while (x >= 0)
        {
            if (CheckAndAddPosToList(new ChessPos(x -= 1, y), true, true, ref valid))
                break;
        }
        x = this.x;
        y = this.y;
        while (y <= 7)
        {
            if (CheckAndAddPosToList(new ChessPos(x, y += 1), true, true, ref valid))
                break;
        }

        return valid;

    }

    public override List<ChessPos> CheckValidKill()
    {
        List<ChessPos> valid = new List<ChessPos>();
        int x = this.x;
        int y = this.y;
        while (x <= 7 && y <= 7)
        {
            if (CheckAndAddPosToList(new ChessPos(x += 1, y += 1), true, true, ref valid, true))
                break;
        }
        x = this.x;
        y = this.y;
        while (x <= 7 && y >= 0)
        {
            if (CheckAndAddPosToList(new ChessPos(x += 1, y -= 1), true, true, ref valid, true))
                break;
        }
        x = this.x;
        y = this.y;
        while (x >= 0 && y <= 7)
        {
            if (CheckAndAddPosToList(new ChessPos(x -= 1, y += 1), true, true, ref valid, true))
                break;
        }
        x = this.x;
        y = this.y;
        while (x >= 0 && y >= 0)
        {
            if (CheckAndAddPosToList(new ChessPos(x -= 1, y -= 1), true, true, ref valid, true))
                break;
        }
        x = this.x;
        y = this.y;
        while (x <= 7)
        {
            if (CheckAndAddPosToList(new ChessPos(x += 1, y), true, true, ref valid, true))
                break;
        }
        x = this.x;
        y = this.y;
        while (y >= 0)
        {
            if (CheckAndAddPosToList(new ChessPos(x, y -= 1), true, true, ref valid, true))
                break;
        }
        x = this.x;
        y = this.y;
        while (x >= 0)
        {
            if (CheckAndAddPosToList(new ChessPos(x -= 1, y), true, true, ref valid, true))
                break;
        }
        x = this.x;
        y = this.y;
        while (y <= 7)
        {
            if (CheckAndAddPosToList(new ChessPos(x, y += 1), true, true, ref valid, true))
                break;
        }

        return valid;
    }


}
