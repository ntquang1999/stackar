using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pawn : Chess
{
    public override void Awake()
    {
        base.Awake();
        type = ChessType.Pawn;
    }

    public override List<ChessPos> CheckValidKill()
    {
        List<ChessPos> valid = new List<ChessPos>();
        if (side == Side.light)
        {
            CheckAndAddPosToList(new ChessPos(x + 1, y + 1), true, true, ref valid, true);
            CheckAndAddPosToList(new ChessPos(x - 1, y + 1), true, true, ref valid, true);

            return valid;
        }
        else
        {
            CheckAndAddPosToList(new ChessPos(x + 1, y - 1), true, true, ref valid, true);
            CheckAndAddPosToList(new ChessPos(x - 1, y - 1), true, true, ref valid, true);

            return valid;
        }
    }

    public override List<ChessPos> CheckValidMove()
    {
        List<ChessPos> valid = new List<ChessPos>();
        if(side == Side.light)
        {
            if (firstMove && !CheckAndAddPosToList(new ChessPos(x, y + 1), false, false, ref valid))
            {
                CheckAndAddPosToList(new ChessPos(x, y + 2), false, true, ref valid);
            }
            CheckAndAddPosToList(new ChessPos(x, y + 1), false, true, ref valid);
            CheckAndAddPosToList(new ChessPos(x + 1, y + 1), true, false, ref valid);
            CheckAndAddPosToList(new ChessPos(x - 1, y + 1), true, false, ref valid);

            return valid;
        }
        else
        {
            if (firstMove && !CheckAndAddPosToList(new ChessPos(x, y - 1), false, false, ref valid))
            {
                CheckAndAddPosToList(new ChessPos(x, y - 2), false, true, ref valid);
            }
            CheckAndAddPosToList(new ChessPos(x, y - 1), false, true, ref valid);
            CheckAndAddPosToList(new ChessPos(x + 1, y - 1), true, false, ref valid);
            CheckAndAddPosToList(new ChessPos(x - 1, y - 1), true, false, ref valid);

            return valid;
        }
        
    }

    
}
