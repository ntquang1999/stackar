using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using UnityEngine.SceneManagement;

public class ChessLobby : MonoBehaviour
{
    [SerializeField] InputField codeInput;
    [SerializeField] GameObject codePanel;
    [SerializeField] GameObject waitingText;

    NetworkController networkController;

    private void Awake()
    {
        networkController = FindObjectOfType<NetworkController>();

        networkController.onJoinedLobby += onJoinedLobby;
        networkController.onJoinedRoom += onJoinedRoom;
        
        waitingText.GetComponent<Text>().text = "Connecting to server...";
        waitingText.SetActive(true);

    }

    private void OnDestroy()
    {
        networkController.onJoinedLobby -= onJoinedLobby;
        networkController.onJoinedRoom -= onJoinedRoom;
    }

    public void onJoinedLobby()
    {
        codePanel.SetActive(true);
        waitingText.SetActive(false);
    }

    public void onJoinedRoom()
    {
        codePanel.SetActive(false);
        if (PhotonNetwork.CurrentRoom.PlayerCount < 2)
            waitingText.GetComponent<Text>().text = "Waiting for opponent...";
        else
            waitingText.GetComponent<Text>().text = "Loading game...";
        waitingText.SetActive(true);
    }

    public void JoinRoom()
    {
        networkController.JoinRoomWithCode(codeInput.text);
    }

    public void Back()
    {
        PhotonNetwork.Disconnect();
        Destroy(NetworkController.Instance.gameObject);
        SceneManager.LoadScene(0);
    }    
}
