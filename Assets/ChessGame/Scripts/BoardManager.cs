using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using UnityEngine.SceneManagement;

public struct ChessPos
{
    public ChessPos(int a, int b)
    {
        x = a;
        y = b;
    }
    public int x;
    public int y;
}

public class BoardManager : MonoBehaviour
{
    private const int CHESS_SIZE = 8;

    [Header("Light chess prefabs")]
    [SerializeField] private GameObject king_light;
    [SerializeField] private GameObject queen_light;
    [SerializeField] private GameObject bishop_light;
    [SerializeField] private GameObject knight_light;
    [SerializeField] private GameObject rook_light;
    [SerializeField] private GameObject pawn_light;
    [Header("Dark chess prefabs")]
    [SerializeField] private GameObject king_dark;
    [SerializeField] private GameObject queen_dark;
    [SerializeField] private GameObject bishop_dark;
    [SerializeField] private GameObject knight_dark;
    [SerializeField] private GameObject rook_dark;
    [SerializeField] private GameObject pawn_dark;
    [Header("Cell prefab")]
    [SerializeField] private GameObject cellPrefab;
    [SerializeField] private GameObject promoteSelectionPanel;
    [SerializeField] private GameObject endgameBoardPanel;
    [SerializeField] private Text endgameMessage;
    [SerializeField] private GameObject playerPrefab;
    public GameObject[,] cell;

    public List<GameObject> chesses = new List<GameObject>();
    public List<ChessPos> invalidKingMove = new List<ChessPos>();
    private List<ChessPos> selectedCell;
    private Chess promotingChess;

    public PlayerController player;
    private Chess kingLight;
    private Chess kingDark;

    public int PromoteSelection = 1;

    public bool MovingPhase = true;
    public Side turn = Side.light;

    private void Awake()
    {
        StartGame();
    }

    private void Update()
    {
        
    }

    private void GenerateBoard(int chessSize)
    {
        cell = new GameObject[chessSize, chessSize];
        for (int i = 0; i < CHESS_SIZE; i++)
            for (int j = 0; j < CHESS_SIZE; j++)
            {
                cell[i, j] = Instantiate(cellPrefab);
                cell[i, j].transform.parent = transform;
                cell[i, j].name = string.Format("X:{0}, Y:{1}", i, j);
                cell[i, j].transform.localPosition = new Vector3(i, 0.01f, j);
                cell[i, j].transform.localScale = new Vector3(1, 1, 1);
                cell[i, j].transform.localRotation = Quaternion.Euler(90,0,0);
                cell[i, j].GetComponent<Cell>().X = i;
                cell[i, j].GetComponent<Cell>().Y = j;
                cell[i, j].tag = "cell";
                cell[i, j].layer = 3;
            }



    }

    private void SpawnChesses()
    {
        SpawnSingleChess(king_light, 3, 0, Side.light);
        SpawnSingleChess(queen_light, 4, 0, Side.light);
        SpawnSingleChess(bishop_light, 2, 0, Side.light);
        SpawnSingleChess(bishop_light, 5, 0, Side.light);
        SpawnSingleChess(knight_light, 1, 0, Side.light);
        SpawnSingleChess(rook_light, 0, 0, Side.light);
        SpawnSingleChess(rook_light, 7, 0, Side.light);
        SpawnSingleChess(knight_light, 6, 0, Side.light);
        for(int i=0; i<8; i++)
        {
            SpawnSingleChess(pawn_light,i, 1, Side.light);
        }

        SpawnSingleChess(king_dark, 3, 7, Side.dark);
        SpawnSingleChess(queen_dark, 4, 7, Side.dark);
        SpawnSingleChess(bishop_dark, 2, 7, Side.dark);
        SpawnSingleChess(bishop_dark, 5, 7, Side.dark);
        SpawnSingleChess(knight_dark, 1, 7, Side.dark);
        SpawnSingleChess(rook_dark, 0, 7, Side.dark);
        SpawnSingleChess(rook_dark, 7, 7, Side.dark);
        SpawnSingleChess(knight_dark, 6, 7, Side.dark);
        for (int i = 0; i < 8; i++)
        {
            SpawnSingleChess(pawn_dark, i, 6, Side.dark);
        }
    }

    private Chess SpawnSingleChess(GameObject chessPrefab, int i, int j, Side side)
    {
        var chess = Instantiate(chessPrefab);
        chess.transform.parent = transform;
        var cellComponent = cell[i, j].GetComponent<Cell>();
        var chessComponent = chess.GetComponent<Chess>();
        chess.transform.localPosition = cellComponent.GetPosition();
        cellComponent.chessStanding = chess;
        chess.transform.localScale = new Vector3(1, 1, 1);

        if (side == Side.light)
        {
            chess.transform.localRotation = Quaternion.identity;
            chess.tag = "chessLight";
        } 
        else
        {
            chess.transform.localRotation = Quaternion.Euler(0,180,0);
            chess.tag = "chessDark";
        }
            
        chess.layer = 3;
        chessComponent.side = side;
        chessComponent.x = i;
        chessComponent.y = j;
        chessComponent.cellReferent = cellComponent;
        if(chessComponent.type == ChessType.Pawn)
            chessComponent.isPromoted += ShowPromoteBoard;
        if (chessComponent.type == ChessType.King)
        {
            if (side == Side.light)
                kingLight = chessComponent;
            else
                kingDark = chessComponent;
        }
            chessComponent.KingIsKilled += ShowEndgameBoard;
        chesses.Add(chess);
        return chessComponent;
    }

    private void SpawnPlayer()
    {
        //if (NetworkController.Instance.localPlayer == null)
        //    player = PhotonNetwork.Instantiate("Player", Vector3.zero, Quaternion.identity).GetComponent<PlayerController>();
        //else
            player = NetworkController.Instance.localPlayer;
    }

    public void ShowEndgameBoard(Side side)
    {
        endgameBoardPanel.SetActive(true);
        if (side == Side.light) side = Side.dark; else side = Side.light;
        endgameMessage.text = string.Format("The {0} side has won the game!", side);
    }

    public void MoveChess(ChessPos startCell, ChessPos endCell)
    {
        bool isValidMove = false;
        foreach(var cell in selectedCell)
        {
            if (cell.x == endCell.x && cell.y == endCell.y)
                isValidMove = true;
        }
        if (!isValidMove) return;

        var startCellComp = cell[startCell.x, startCell.y].GetComponent<Cell>();
        var endCellComp = cell[endCell.x, endCell.y].GetComponent<Cell>();
        var chess = startCellComp.chessStanding.GetComponent<Chess>();
        if (startCellComp.chessStanding != null)
        {
            if (endCellComp.chessStanding != null)
                endCellComp.chessStanding.GetComponent<Chess>().Depsawn();

            endCellComp.chessStanding = startCellComp.chessStanding;
            chess.x = endCellComp.X;
            chess.y = endCellComp.Y;
            chess.cellReferent = endCellComp;
            startCellComp.chessStanding = null;

            MovingPhase = false;
            chess.Move(new Vector3(endCell.x, 0, endCell.y));
            chess.isDoneMoving += SwitchTurn;
        }
    }

    public void ShowPromoteBoard(Chess chess)
    {
        promoteSelectionPanel.SetActive(true);
        promotingChess = chess;
        chess.isPromoted -= ShowPromoteBoard;
    }

    public void Promote(int type)
    {
        promoteSelectionPanel.SetActive(false);
        PromoteSelection = type;
        var currentCell = promotingChess.cellReferent;
        var currentSide = promotingChess.side;
        promotingChess.Depsawn();
        switch(PromoteSelection)
        {
            case 1:
                if (currentSide == Side.light)
                    promotingChess = SpawnSingleChess(queen_light, currentCell.X, currentCell.Y, currentSide);
                else
                    promotingChess = SpawnSingleChess(queen_dark, currentCell.X, currentCell.Y, currentSide);
                break;
            case 2:
                if (currentSide == Side.light)
                    promotingChess = SpawnSingleChess(bishop_light, currentCell.X, currentCell.Y, currentSide);
                else
                    promotingChess = SpawnSingleChess(bishop_dark, currentCell.X, currentCell.Y, currentSide);
                break;
            case 3:
                if (currentSide == Side.light)
                    promotingChess = SpawnSingleChess(knight_light, currentCell.X, currentCell.Y, currentSide);
                else
                    promotingChess = SpawnSingleChess(knight_dark, currentCell.X, currentCell.Y, currentSide);
                break;
            case 4:
                if (currentSide == Side.light)
                    promotingChess = SpawnSingleChess(rook_light, currentCell.X, currentCell.Y, currentSide);
                else
                    promotingChess = SpawnSingleChess(rook_dark, currentCell.X, currentCell.Y, currentSide);
                break;
        }
        SwitchTurn(promotingChess);
    }

    public void SwitchTurn(Chess chess)
    {
        player.gameObject.GetComponent<PhotonView>().RPC("SwitchTurn", RpcTarget.All);
        chess.isDoneMoving -= SwitchTurn;
        MovingPhase = true;
        if (turn == Side.light)
            turn = Side.dark;
        else
            turn = Side.light;
    }

    public void SetChessIgnoreRaycast(bool status)
    {
        if(status)
        {
            foreach (var chess in chesses)
            {
                chess.layer = 2;
            }
        }
        else
        {
            foreach (var chess in chesses)
            {
                chess.layer = 3;
            }
        }
    }

    public void ShowValidMove(Chess chess)
    {
        selectedCell = chess.CheckValidMove();
        foreach (var chesspos in selectedCell)
        {
            cell[chesspos.x, chesspos.y].GetComponent<Cell>().SetSelection(true, 0);
        }
    }

    public void HideSelectedCell()
    {
        foreach (var chesspos in selectedCell)
        {
            cell[chesspos.x, chesspos.y].GetComponent<Cell>().SetSelection(false, 0);
        }
    }

    public void PerformKingInvalidMove(Side side)
    {
        invalidKingMove.Clear();
        foreach(var chessObj in chesses)
        {
            Chess chess = chessObj.GetComponent<Chess>();

            if (chess.side != side)
            {
                foreach(var invalidPos in chess.CheckValidKill() )
                {
                    bool alreadyIncluded = false;
                    foreach(var pos in invalidKingMove)
                    {
                        if (pos.x == invalidPos.x && pos.y == invalidPos.y)
                            alreadyIncluded = true;
                    }
                    if (!alreadyIncluded)
                        invalidKingMove.Add(invalidPos);
                }
            }
        }
    } 

    public void StartGame()
    {
        GenerateBoard(CHESS_SIZE);
        SpawnChesses();
        SpawnPlayer();
    }
    
    public void RestartTheGame()
    {
        PhotonNetwork.Disconnect();
        Destroy(NetworkController.Instance.gameObject);
        SceneManager.LoadScene(0);
    }
}
