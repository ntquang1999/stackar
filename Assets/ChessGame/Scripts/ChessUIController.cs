using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChessUIController : MonoBehaviour
{
    public Text state;
    public BoardManager boardManager;
    private void Update()
    {
        if (boardManager.turn == Side.light)
            state.text = "Light";
        if (boardManager.turn == Side.dark)
            state.text = "Dark";
        if (!boardManager.MovingPhase)
            state.text = "Moving";
    }
}
