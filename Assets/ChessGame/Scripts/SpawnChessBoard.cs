using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnChessBoard : MonoBehaviour
{
    [SerializeField] GameObject chessboardPrefab;

    public bool spawnDelay = false;

    private void Start()
    {
        if (spawnDelay)
            StartCoroutine(SpawnAfterDelay(5f));
        else
            Instantiate(chessboardPrefab);
    }

    IEnumerator SpawnAfterDelay(float delay)
    {
        yield return new WaitForSeconds(delay);
        Instantiate(chessboardPrefab);
    }
}
