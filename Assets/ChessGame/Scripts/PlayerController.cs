using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum Side
{
    light = 0,
    dark = 1,
}

public class PlayerController : MonoBehaviour
{
    public Side side = Side.light;
    public bool MyTurn;


    [SerializeField] private string chessTag;
    [SerializeField] private string cellTag = "cell";
    [SerializeField] private Material highlightMat;

    private GameObject SelectedChess;
    private bool chessSelected = false;
    private Material oldChessMaterial;
    private Material oldCellMaterial;
    [SerializeField]private BoardManager boardManager;
    int chessLayerMask;
    private PhotonView photonView;
    private Transform selection;
    private bool isTouchControl = false;

    [PunRPC]
    void SetProperties()
    {
        if (photonView.IsMine)
        {
            if(PhotonNetwork.IsMasterClient)
            {
                side = Side.light;
                chessTag = "chessLight";
                MyTurn = true;
            }
            else
            {
                side = Side.dark;
                chessTag = "chessDark";
                MyTurn = false;
            }
        }
        else
        {
            if (PhotonNetwork.IsMasterClient)
            {
                side = Side.dark;
                chessTag = "chessDark";
                MyTurn = false;
            }
            else
            {
                side = Side.light;
                chessTag = "chessLight";
                MyTurn = true;
            }
        }
        
    }

    [PunRPC]
    void SwitchTurn()
    {
        MyTurn = !MyTurn;
    }

    [PunRPC]
    void CacheBoardComponent()
    {
        boardManager = FindObjectOfType<BoardManager>();
        isTouchControl = FindObjectOfType<PlayerSpawner>().isTouchVersion;
    }

    private void Awake()
    {
        //boardManager = FindObjectOfType<BoardManager>();
        chessLayerMask = LayerMask.GetMask("Raycast");
        photonView = GetComponent<PhotonView>();
    }

    private void Start()
    {
        
        
    }

    private void Update()
    {
        if (!photonView.IsMine) return;

        if (boardManager == null)
            return;

        if (!boardManager.MovingPhase)
            return;

        if (!MyTurn)
            return;

        if(isTouchControl)
        {
            if (Input.touchCount == 0)
            {
                return;
            }

            Touch touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Began)
            {
                var ray = Camera.main.ScreenPointToRay(touch.position);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit, 10000000f, chessLayerMask))
                {
                    selection = hit.transform;
                    if (selection.CompareTag(chessTag) && !chessSelected)
                    {

                        photonView.RPC("SelectChess", RpcTarget.All, ConvertSelectionToChessIndex(selection));
                    }
                    else
                    if (selection.CompareTag(cellTag) && chessSelected)
                    {
                        ChessPos temp = ConvertSelectionToCellPos(selection);
                        photonView.RPC("MoveChess", RpcTarget.All, temp.x, temp.y);
                        photonView.RPC("deselectChess", RpcTarget.All);
                    }
                    else
                        photonView.RPC("deselectChess", RpcTarget.All);
                }
                else
                    photonView.RPC("deselectChess", RpcTarget.All);
            }
        }
        else
        {
            if(Input.GetMouseButtonDown(0))
            {
                var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit, 10000000f, chessLayerMask))
                {
                    selection = hit.transform;
                    if (selection.CompareTag(chessTag) && !chessSelected)
                    {

                        photonView.RPC("SelectChess", RpcTarget.All, ConvertSelectionToChessIndex(selection));
                    }
                    else
                    if (selection.CompareTag(cellTag) && chessSelected)
                    {
                        ChessPos temp = ConvertSelectionToCellPos(selection);
                        photonView.RPC("MoveChess", RpcTarget.All, temp.x, temp.y);
                        photonView.RPC("deselectChess", RpcTarget.All);
                    }
                    else
                        photonView.RPC("deselectChess", RpcTarget.All);
                }
                else
                    photonView.RPC("deselectChess", RpcTarget.All);
            }
        }
        
    }

    private int ConvertSelectionToChessIndex(Transform selection)
    {
        for(int i =0; i< boardManager.chesses.Count; i++)
        {
            if (selection.gameObject == boardManager.chesses[i])
                return i;
        }
        return -1;
    }

    private ChessPos ConvertSelectionToCellPos(Transform selection)
    {
        return selection.gameObject.GetComponent<Cell>().GetPos();
    }

    [PunRPC]
    void SelectChess(int chessIndex)
    {
        var chesses = boardManager.chesses;
        var selectionRenderer = chesses[chessIndex].GetComponent<Renderer>();
        oldChessMaterial = selectionRenderer.material;
        selectionRenderer.material = highlightMat;
        chessSelected = true;
        SelectedChess = chesses[chessIndex].gameObject;
        boardManager.SetChessIgnoreRaycast(true);
        Chess chessComponent = SelectedChess.GetComponent<Chess>();
        chessComponent.isSelected = true;

        if (chessComponent.type == ChessType.King)
            boardManager.PerformKingInvalidMove(side);

        boardManager.ShowValidMove(chessComponent);
    }

    [PunRPC]
    void MoveChess(int x, int y)
    {
        boardManager.MoveChess(SelectedChess.GetComponent<Chess>().cellReferent.GetPos(), new ChessPos(x,y));
    }

    [PunRPC]
    void deselectChess()
    {
        if (chessSelected)
        {
            boardManager.SetChessIgnoreRaycast(false);
            SelectedChess.GetComponent<Renderer>().material = oldChessMaterial;
            SelectedChess.GetComponent<Chess>().isSelected = false;
            SelectedChess = null;
            chessSelected = false;
            boardManager.HideSelectedCell();
        }
    }
}
