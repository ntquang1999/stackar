using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ChessType
{
    King = 0,
    Queen = 1,
    Bishop = 2,
    Knight = 3,
    Rook = 4, 
    Pawn = 5
}
public class Chess : MonoBehaviour
{
    public Side side;
    public ChessType type;
    public int x;
    public int y;

    public Cell cellReferent;

    public bool isMoving;
    public bool firstMove = true;
    protected Vector3 _targetPos;
    protected Vector3 selectedPos;
    protected float minDistance = 0.01f;
    protected float speed = 10;
    public bool isSelected = false;
    public bool isDespawning = false;

    public event Action<Chess> isDoneMoving;
    public event Action<Chess> isPromoted;
    public event Action<Side> KingIsKilled;

    public BoardManager boardManager;

    public virtual void Awake()
    {
        boardManager = FindObjectOfType<BoardManager>();
    }

    private void OnDestroy()
    {
        
    }

    public void Depsawn()
    {
        boardManager.chesses.Remove(gameObject);
        Destroy(gameObject, 0.3f);
        isDespawning = true;
        if (type == ChessType.King)
            KingIsKilled.Invoke(side);
    }    

    public void Move(Vector3 targetPos)
    {
        firstMove = false;
        isMoving = true;
        _targetPos = targetPos;
    }

    protected void Update()
    {
        if(isMoving)
        {
            if (Vector3.Distance(transform.localPosition, _targetPos) > minDistance)
                transform.localPosition = Vector3.Lerp(transform.localPosition, _targetPos, Time.deltaTime * speed);
            else
            {
                transform.localPosition = _targetPos;
                isMoving = false;
                if (type == ChessType.Pawn && ((side == Side.light && y == 7) || (side == Side.dark && y == 0)))
                    isPromoted?.Invoke(this);
                else
                    isDoneMoving?.Invoke(this);
            }
        }
        else if(isSelected)
        {
            selectedPos = new Vector3(transform.localPosition.x, 0.75f, transform.localPosition.z);
            if (Vector3.Distance(transform.localPosition, selectedPos) > minDistance)
                transform.localPosition = Vector3.Lerp(transform.localPosition, selectedPos, Time.deltaTime * speed);
            else
            {
                transform.localPosition = selectedPos;
            }
        }
        else if(!isDespawning)
        {
            Vector3 basePos = cellReferent.GetPosition();
            if (Vector3.Distance(transform.localPosition, basePos) > minDistance)
                transform.localPosition = Vector3.Lerp(transform.localPosition, basePos, Time.deltaTime * speed);
            else
            {
                transform.localPosition = basePos;
            }
        }

        if(isDespawning)
        {
            var material = GetComponent<Renderer>().material;
            MaterialExtensions.ToFadeMode(material);
            Color color = material.color;
            selectedPos = new Vector3(transform.localPosition.x, 1.5f, transform.localPosition.z);
            if (Vector3.Distance(transform.localPosition, selectedPos) > minDistance)
            {
                transform.localPosition = Vector3.Lerp(transform.localPosition, selectedPos, Time.deltaTime * speed/3);
                color.a = Mathf.Lerp(color.a, 0, Time.deltaTime * speed);
            }
            else
            {
                transform.localPosition = selectedPos;
            }
            material.color = color;
        }

    }
    public virtual List<ChessPos> CheckValidMove()
    {
        return new List<ChessPos>();
    }

    public virtual List<ChessPos> CheckValidKill()
    {
        return new List<ChessPos>();
    }

    protected bool CheckAndAddPosToList(ChessPos pos, bool addIfTrue, bool addIfFlase, ref List<ChessPos> list, bool checkingForKing = false)
    {
        if (pos.x < 0 || pos.x > 7 || pos.y < 0 || pos.y > 7) return false;
        var targetCell = boardManager.cell[pos.x, pos.y].GetComponent<Cell>().chessStanding;
        if (type != ChessType.King)
        {
            if (targetCell != null)
            {
                if (targetCell.GetComponent<Chess>().side != side)
                { 
                    if (addIfTrue) list.Add(pos); 
                }
                else if (checkingForKing)
                    if (addIfTrue) list.Add(pos);
                return true;
            }
            else
            {
                if (addIfFlase) list.Add(pos);
                return false;
            }
        }
        else
        {
            foreach(var cell in boardManager.invalidKingMove)
            {
                if(pos.x == cell.x && pos.y == cell.y)
                {
                    if (targetCell != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            if (targetCell != null)
            {
                if (targetCell.GetComponent<Chess>().side != side)
                { 
                    if (addIfTrue) list.Add(pos);
                }
                else if (checkingForKing)
                    if (addIfTrue) list.Add(pos);
                return true;
            }
            else
            {
                if (addIfFlase) list.Add(pos);
                return false;
            }
        }
        
    }


}

