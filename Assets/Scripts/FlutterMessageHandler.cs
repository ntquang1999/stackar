using FlutterUnityIntegration;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlutterMessageHandler : MonoBehaviour
{
    UnityMessageManager unityMessageManager;
    private void Awake()
    {
        unityMessageManager = GetComponent<UnityMessageManager>();
    }

    public void SendMessageToFlutter(string message)
    {
        unityMessageManager.SendMessageToFlutter(message);
    }
}
